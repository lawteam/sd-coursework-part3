class Options(object):

    def __init__(self):
        self.prompts = list()
        self.acceptables = list()

    def add_option(self, prompt, trigger):
        self.prompts.append("[%s] %s" % (trigger, prompt))
        self.acceptables.append(trigger)

