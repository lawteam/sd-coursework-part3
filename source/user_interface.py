import os


class UserInterface(object):
    """
    An object that deals with the interactions with users, reporting details and
    accepting input
    """

    @staticmethod
    def clear_console():
        """
        Clear the screen ready for the next display
        :return:
        """
        os.system('cls' if os.name == 'nt' else 'clear')

    def display_available(self, central_set, supplement):
        """
        Report the available contents of the central set and the supplement
        :param central_set:
        :param supplement:
        :return:
        """
        print
        self._underline_("Central Line")
        for card in central_set:
            print self.card_to_string_for_purchase(card)
        print
        self._underline_("Supplement")
        if supplement is not None:
            print self.card_to_string_for_purchase(supplement)

    @staticmethod
    def get_response(prompt, acceptables):
        """
        Prompt the user and wait for a response that is one of the supplied possible
        values
        NOTE: the checking of the input is case-insensitive
        NOTE: the returned response will always be UPPER CASE
        If case-sensitive responses are required then this routine needs to be overridden
        :param prompt: The prompt supplied to the user
        :param acceptables: a list of acceptable responses
        :return: the upper-case value of the response from the user
        """
        _upper_acceptables = []
        for char in acceptables:
            _upper_acceptables.append(char.upper())
        waiting = True
        while waiting:
            choice = raw_input(prompt)
            if len(_upper_acceptables) == 0:
                return ""
            if choice.upper() in _upper_acceptables:
                return choice.upper()
            else:
                print "Unrecognised choice"

    def get_options_response(self, prompt, options, acceptables):
        """
        Prompt the user and wait for a response that is one of the supplied possible
        values
        NOTE: the checking of the input is case-insensitive
        NOTE: the returned response will always be UPPER CASE
        If case-sensitive responses are required then this routine needs to be overridden
        :param prompt: The prompt supplied to the user
        :param options: A list of possible options
        :param acceptables: a list of acceptable responses
        :return: the upper-case value of the response from the user
        """
        print
        print "Choose an Action"
        for option in options:
            print option
        return self.get_response(prompt, acceptables)

    def _print_card_(self, card):
        """
        Print the details of a Card object to STDOUT
        :param card:
        :return:
        """
        print self.card_to_string_for_purchase(card)

    @staticmethod
    def _underline_(string):
        """
        Print the string and underline it
        :param string:
        :return:
        """
        print string
        print "-" * len(string)

    @staticmethod
    def card_to_string_for_purchase(card):
        """
        Convert a Card object to a string representation in buying context (Central/Supplement)
        :rtype: String
        :param card:
        :return:
        """
        return '%s - Cost: %s (Attack %s / Money %s)' % \
               (card.name, card.cost, card.strength, card.value)

    @staticmethod
    def card_to_string(card):
        """
        Convert a Card object to a string representation for display in a hand
        :param card:
        :return:
        """
        return '%s (Attack %s / Money %s)' % (card.name, card.strength, card.value)

    @staticmethod
    def show_health(player_one, computer):
        """
        Show the current health of the two protagonists
        :param player_one:
        :param computer:
        :return:
        """
        print 'Player 1 Health: %s (%d)' % ('*' * player_one.health, player_one.health)
        print 'Computer Health: %s (%d)' % ('*' * computer.health, computer.health)

    def show_status(self, player):
        """
        Show the status of the player's turn
        :param player:
        :return:
        """
        print
        self._underline_("%s Turn" % player.name)
        hand_stats = 'Attack Strength %d, Monetary Value %d' %(player.attack_strength, player.monetary_value)
        print "%s %s %s" % (" " * 3, '{:^40}'.format(""), '{:^40}'.format(hand_stats))
        print
        print "%s %s %s" % (" " * 3, '{:^40}'.format("Hand"), '{:^40}'.format("Active"))
        top = max(len(player.hand), len(player.active))
        for x in range(0, top):
            number_string = "[%d]" % x if len(player.hand) > x else ''
            hand_card = self.card_to_string(player.hand[x]) if len(player.hand) > x else ''
            active_card = self.card_to_string(player.active[x]) if len(player.active) > x else ''
            print "%3s %-40s %-40s" % (number_string, hand_card, active_card)

    @staticmethod
    def show_player_name(player):
        """
        Display the Player name (used at the top of each screen)
        :param player:
        :return:
        """
        print player.name
        print "-" * len(player.name)
