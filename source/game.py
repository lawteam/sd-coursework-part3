import random
import re
from card import Archer, Assassin, Baker, Caravan, Catapault, Crossbowman, Knight, Levy, \
    Merchant, Tailor, Thief, Thug, Swordsman
from player import Player
from user_interface import UserInterface
from computer_agent import ComputerAgent
from options import Options


class Game(object):
    def __init__(self):
        deck_start = [
            4 * [Archer()],
            4 * [Baker()],
            3 * [Swordsman()],
            2 * [Knight()],
            3 * [Tailor()],
            3 * [Crossbowman()],
            3 * [Merchant()],
            4 * [Thug()],
            4 * [Thief()],
            2 * [Catapault()],
            2 * [Caravan()],
            2 * [Assassin()]
        ]
        deck = list()
        for block in deck_start:
            for card in block:
                deck.append(card)

        random.shuffle(deck)
        self.deck = deck

        self.central = self.deck[0:5]

        supplement = list()
        for x in range(0, 10):
            supplement.append(Levy())
        self.supplement = supplement

        self.player_one = Player('Player 1')
        self.computer = Player('Computer')

        self.player_one.opponent = self.computer
        self.computer.opponent = self.player_one

        self.ui = UserInterface()

        self.computer_agent = ComputerAgent(self.computer, self)

    def purchase_card(self, index, player):
        """
        A player can choose to purchase a card from Central set if they have sufficient funds. If the
        funds are sufficient, return the Card object and update Deck and Central accordingly. If not, return None
        :param player:
        :param index: the index number (0-based) in the Central stack that we are requesting
        :return: the purchased Card (or None)
        """
        card = self.central[index]
        if player.monetary_value < card.cost:
            return None
        self.deck.pop(index)
        self.central = self.deck[0:5]
        player.discard.append(card)
        player.monetary_value -= card.cost
        return card

    def purchase_supplement(self, player):
        """
        A player can choose to purchase a card from the Supplement if they have sufficient funds. If the
        funds are sufficient, return the Card object and update Supplement accordingly. If not, return None
        :param player:
        :return: the purchased Card (or None)
        """
        card = self.supplement[0]
        if player.monetary_value < card.cost:
            return None
        self.supplement.pop()
        player.discard.append(card)
        player.monetary_value -= card.cost
        return card

    def game_still_active(self):
        """
        Check to see if either or both players have reached zero health or if we have
        run out of cards in the central line
        Return false if that is the case
        :return: True is both players have health > 0, False otherwise
        """
        return (self.player_one.is_alive() and self.computer.is_alive()) and len(self.central) > 0

    def show_health_status(self):
        """
        Show the current health status of the two players
        :return:
        """
        self.ui.show_health(self.player_one, self.computer)

    def run(self):
        """
        The main routine controlling the flow of logic through the game
        :return:
        """
        self.ui.clear_console()
        self.ui.display_available(self.central, self.supplement[0])

        response = self.ui.get_response("Do you want to play this game? ", ['Y', 'N'])
        if response == "N":
            return False

        self.computer_agent.style = self.ui.get_response(
            "Do you want an aggressive (A) opponent or an acquisative (Q) opponent ",
            ['A', 'Q'])

        while self.game_still_active():
            self.run_player_turn(self.player_one, self.ui)
            self.run_player_turn(self.computer, self.computer_agent)
        self.report_outcome()

        return True

    def report_outcome(self):
        """
        Called when the game is over to report who won and who lost
        :return:
        """
        self.ui.clear_console()
        self.ui.show_health(self.player_one, self.computer)
        print
        print self._get_result_string()
        self.ui.get_response("Press return to continue", [])

    def _get_result_string(self):
        """
        Return a string reporting the result of the game
        :return:
        """
        draw = "Well done. An honourable draw"
        win = "Congratulations! You won!"
        lose = "Hard Luck. The Computer won this time!"
        no_cards_left = "No Cards Left.\n%s (based on %s score)"

        computer = self.computer
        player = self.player_one

        if (computer.health == 0) and (player.health == 0):
            return draw

        if computer.health == 0:
            return win

        if player.health == 0:
            return lose

        if computer.health > player.health:
            return no_cards_left % (lose, 'health')

        if computer.health < player.health:
            return no_cards_left % (win, 'health')

        return no_cards_left % (draw, 'health')

    def run_player_turn(self, player, ui_agent):
        """
        Manage an individual turn, be it the human user or the computer user playing.
        :param player:
        :param ui_agent:
        :return:
        """
        player.begin_turn()
        while player.in_play:
            options = self.get_available_options(player)
            ui_agent.clear_console()
            ui_agent.show_player_name(player)
            self.show_health_status()
            supplement = self._get_supplement()
            ui_agent.display_available(self.central, supplement)
            ui_agent.show_status(player)
            choice = ui_agent.get_options_response('Choose Action :', options.prompts, options.acceptables)
            self.handle_choice(player, choice)

    def handle_choice(self, player, choice):
        """
        Deal with the user's input
        :param player: the player object against which the choice must be registered
        :param choice: the chosen option
        :return:
        """
        if choice == 'P':
            player.play_all()
            return

        num_match = re.match("^[0-4]$", choice)
        is_number = num_match is not None
        if is_number:
            index = num_match.group(0)
            player.play_card(int(index))
            return

        buy_match = re.match("^B([0-4])$", choice)
        if buy_match is not None:
            index = buy_match.group(1)
            self.purchase_card(int(index), player)
            return

        if choice == 'S':
            self.purchase_supplement(player)
            return

        if choice == 'A':
            player.launch_attack()
            return

        if choice == 'E':
            player.end_turn()
            return

    def get_available_options(self, player):
        """
        Build a list of possible actions based on supplied player's hand and active set
        and the central and supplement card lists for this game
        :param player:
        :return: An Options object
        """
        options = Options()
        # if there are cards in the hand, then Play All is an option
        if len(player.hand) > 0:
            options.add_option('Play All', 'P')

        # For each card in the hand, playing that into the active area is an option
        count = 0
        for card in player.hand:
            options.add_option('Play card %s' % self.ui.card_to_string(card), str(count))
            count += 1

        # If the Active hand has any strength, then attack is an option
        if player.attack_strength > 0:
            options.add_option('Attack', 'A')

        # If the Active hand has any monetary value then purchasing any card in the central
        # or supplement whose cost is <= the monetary value is an option
        value = player.monetary_value
        if value > 0:
            count = 0
            for card in self.central:
                if player.monetary_value >= card.cost:
                    options.add_option('Buy card %s from central pile' % self.ui.card_to_string_for_purchase(card),
                                       'B' + str(count))
                count += 1
            supplement = self._get_supplement()
            if supplement is not None and player.monetary_value >= supplement.cost:
                options.add_option('Buy Supplemental card %s' % self.ui.card_to_string_for_purchase(supplement), 'S')

        # Ending the turn is always an option
        options.add_option('End turn', 'E')
        return options

    def _get_supplement(self):
        """
        Wrapper routine to extract the supplement object as the top most card
        from the supplement stack
        :return:
        """
        return None if len(self.supplement) == 0 else self.supplement[0]


if __name__ == '__main__':

    carry_on = True
    while carry_on:
        game = Game()

        carry_on = game.run()
