import random
from card import Squire, Serf


class Player(object):
    def __init__(self, name):
        self.name = name
        self.health = 30
        deck = list()
        for i in range(0, 2):
            deck.append(Squire())
        for i in range(0, 8):
            deck.append(Serf())
        random.shuffle(deck)
        self.deck = deck
        discard = list()
        self.discard = discard
        hand = list()
        self.hand = hand
        active = list()
        self.active = active
        self.in_play = False
        self.attack_strength = 0
        self.monetary_value = 0
        self.opponent = None

    def reduce_health(self, amount):
        """
        :param amount: the amount by which we want to reduce the health score
        :return:
        Health can never be reduced below a score of zero
        """
        new_value = max(0, self.health - int(amount))
        self.health = new_value

    def is_alive(self):
        """
        :return: if health is > 0 then we are still alive
        """
        return self.health > 0

    def begin_turn(self):
        """
        Starts a turn by taking the top 5 cards from the Deck and moving them into the Hand
        :return:
        """
        if self.in_play:
            raise LogicError()
        deck_extract = min(5, len(self.deck))
        for x in range(deck_extract):
            self.hand.append(self.deck.pop())
        if deck_extract < 5:
            random.shuffle(self.discard)
            self.deck = self.discard
            self.discard = []
            for x in range(deck_extract, 5):
                self.hand.append(self.deck.pop())
        self.in_play = True

    def end_turn(self):
        """
        Completes a turn by putting the Active and Hand cards into the Discard
        :return:
        """
        if not self.in_play:
            raise LogicError()
        active_num = len(self.active)
        for x in range(active_num):
            self.discard.append(self.active.pop())
        hand_num = len(self.hand)
        for x in range(hand_num):
            self.discard.append(self.hand.pop())
        self.attack_strength = 0
        self.monetary_value = 0
        self.in_play = False

    def play_card(self, index):
        """
        Play a card into the Active area from the Hand
        :param index:
        :return:
        """
        if index < len(self.hand):
            card = self.hand.pop(index)
            self._add_strength(card.strength)
            self._add_value(card.value)
            self.active.append(card)

    def play_all(self):
        """
        Play all the cards remaining in the current hand into the Active Set
        :return:
        """
        while len(self.hand) > 0:
            self.play_card(0)

    def launch_attack(self):
        """
        Attack the opponent by reducing Health by the attack strength and set the
        attack strength to zero
        :return:
        """
        if self.attack_strength > 0:
            if self.opponent is not None:
                self.opponent.reduce_health(self.attack_strength)
                self.attack_strength = 0

    def _add_strength(self, increment):
        newvalue = self.attack_strength + increment
        self.attack_strength = newvalue

    def _add_value(self, increment):
        newvalue = self.monetary_value + increment
        self.monetary_value = newvalue


class LogicError(Exception):
    """
    Class that we use to indicate that something went wrong in setting up
    or playing a turn
    """

    def __init__(self):
        pass
