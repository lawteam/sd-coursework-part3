from unittest import TestCase
from card import Catapault


class TestCatapault(TestCase):
    def test___init__(self):
        instance = Catapault()
        self.assertEqual('Catapault', instance.name, 'Name should be "Catapault" but was "' + instance.name + '"')
        self.assertEqual(7, instance.strength, 'Strength of a/an Catapault should be 7 but was ' + str(instance.strength))
        self.assertEqual(0, instance.value, 'Value of a/an Catapault should be 0 but was ' + str(instance.value))
        self.assertEqual(6, instance.cost, 'Cost of a/an Catapault should be 6 but was ' + str(instance.cost))

