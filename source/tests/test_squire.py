from unittest import TestCase
from card import Squire


class TestSquire(TestCase):
    def test___init__(self):
        instance = Squire()
        self.assertEqual('Squire', instance.name, 'Name should be "Squire" but was "' + instance.name + '"')
        self.assertEqual(1, instance.strength, 'Strength of a/an Squire should be 1 but was ' + str(instance.strength))
        self.assertEqual(0, instance.value, 'Value of a/an Squire should be 0 but was ' + str(instance.value))
        self.assertEqual(0, instance.cost, 'Cost of a/an Squire should be 0 but was ' + str(instance.cost))

