from unittest import TestCase
from game import Game
from card import Archer, Baker, Knight
from player import Player
from user_interface import UserInterface


class TestGame(TestCase):
    def test___init__deck(self):
        """
        Game object should have a deck with a characteristic set of cards in it
        :return:
        """
        instance = Game()
        deck = instance.deck
        self.assertTrue(len(deck) == 36, "Deck should have 36 cards")
        counter = dict()
        for card in deck:
            name = card.name
            counter[name] = counter.get(name, 0) + 1
        self.assertEqual(4, counter['Archer'], "Should be 4 Archer cards in the deck")
        self.assertEqual(4, counter['Baker'], "Should be 4 Baker cards in the deck")
        self.assertEqual(3, counter['Swordsman'], "Should be 3 Swordsman cards in the deck")
        self.assertEqual(2, counter['Knight'], "Should be 2 Knight cards in the deck")
        self.assertEqual(3, counter['Tailor'], "Should be 3 Tailor cards in the deck")
        self.assertEqual(3, counter['Crossbowman'], "Should be 3 Crossbowman cards in the deck")
        self.assertEqual(3, counter['Merchant'], "Should be 3 Merchant cards in the deck")
        self.assertEqual(4, counter['Thug'], "Should be 4 Thug cards in the deck")
        self.assertEqual(4, counter['Thief'], "Should be 4 Thief cards in the deck")
        self.assertEqual(2, counter['Catapault'], "Should be 2 Catapault cards in the deck")
        self.assertEqual(2, counter['Caravan'], "Should be 2 Caravan cards in the deck")
        self.assertEqual(2, counter['Assassin'], "Should be 2 Assassin cards in the deck")

    def test___init__supplement(self):
        """
        Game object should have a supplement with a characteristic set of cards in it
        :return:
        """
        instance = Game()
        supplement = instance.supplement
        self.assertTrue(len(supplement) == 10, "Supplement should have 10 cards")
        counter = dict()
        for card in supplement:
            name = card.name
            counter[name] = counter.get(name, 0) + 1
        self.assertEqual(10, counter['Levy'], "Should be 10 Levy cards in the Supplement")

    def test___init__central(self):
        """
        Game object should have an central set containing 5 cards. Those 5 cards should be the top 5 from the deck
        :return:
        """
        instance = Game()
        central = instance.central
        self.assertTrue(len(central) == 5, "Central should contain 5 cards")
        self.assertListEqual(central, instance.deck[0:5], "central should be the top 5 from the deck")

    def test___init__player_one(self):
        """
        Game object should have a Player 1 object
        :return:
        """
        instance = Game()
        player_one = instance.player_one
        self.assertIsNotNone(player_one, "Player one should be a real object")

    def test___init__computer(self):
        """
        Game object should have a Computer object
        :return:
        """
        instance = Game()
        computer = instance.computer
        self.assertIsNotNone(computer, "Computer player should be a real object")

    def test_purchase_valid(self):
        """
        Try to purchase a Card with sufficient funds.
        Should return the Card
        Should remove the Card from the Deck
        Should remove the Card from the Central set
        :return:
        """
        instance = Game()
        my_deck = [Archer(), Archer(), Knight(), Baker(), Archer(), Archer(), Archer()]
        instance.deck = my_deck
        instance.central = my_deck[0:5]
        fake_player = Player('fake')
        fake_player.monetary_value = 4
        card = instance.purchase_card(3, fake_player)
        self.assertIsNotNone(card, "Should get back a real card")
        self.assertTrue(len(fake_player.discard) == 1, "Discard should contain the purchased Card")
        self.assertEqual(card, fake_player.discard[0], "Should be the card we just bought")
        self.assertTrue(isinstance(card, Baker), "Card should be a Baker")
        self.assertEqual(6, len(instance.deck), "Should be 6 cards remaining in the deck")
        self.assertEqual(5, len(instance.central), "Should be 5 cards in the central set")
        self.assertListEqual(instance.central, instance.deck[0:5], "central should be the top 5 from the deck")
        for card in instance.deck:
            if isinstance(card, Baker):
                self.fail("Shouldn't be a Baker card in the deck")

    def test_purchase_invalid(self):
        """
        Try to purchase a Card with insufficient funds.
        Should return None
        Should leave the Deck and the Central set unchanged
        :return:
        """
        instance = Game()
        my_deck = [Archer(), Archer(), Knight(), Baker(), Archer(), Archer(), Archer()]
        instance.deck = my_deck
        instance.central = my_deck[0:5]
        fake_player = Player('fake')
        fake_player.monetary_value = 4
        card = instance.purchase_card(2, fake_player)
        self.assertIsNone(card, "Should NOT get back a real card")
        self.assertEqual(4, fake_player.monetary_value, "Monetary value should be unchanged")
        self.assertEqual(7, len(instance.deck), "Should be 6 cards remaining in the deck")
        self.assertEqual(5, len(instance.central), "Should be 5 cards in the central set")
        self.assertListEqual(instance.central, instance.deck[0:5], "central should be the top 5 from the deck")
        saw_knight = False
        for card in instance.deck:
            if isinstance(card, Knight):
                saw_knight = True
        self.assertTrue(saw_knight, "Should be a Knight remaining in the deck")

    def test_purchase_valid_short(self):
        """
        Try to purchase a Card with sufficient funds where the deck is less than 5.
        Should return the Card
        Should remove the Card from the Deck
        Should remove the Card from the Central set
        :return:
        """
        instance = Game()
        my_deck = [Archer(), Archer(), Knight(), Baker()]
        instance.deck = my_deck
        instance.central = my_deck[0:5]
        fake_player = Player('fake')
        fake_player.monetary_value = 4
        card = instance.purchase_card(3, fake_player)
        self.assertIsNotNone(card, "Should get back a real card")
        self.assertTrue(isinstance(card, Baker), "Card should be a Baker")
        self.assertEqual(3, len(instance.deck), "Should be 3 cards remaining in the deck")
        self.assertEqual(3, len(instance.central), "Should be 3 cards in the central set")
        self.assertListEqual(instance.central, instance.deck[0:5],
                             "central should match the top 5 from the deck (which should only be 3)")
        for card in instance.deck:
            if isinstance(card, Baker):
                self.fail("Shouldn't be a Baker card in the deck")

    def test_has_user_interface(self):
        """
        By default, we have a terminal-based (STDIN/STDOUT) user interface object
        :return:
        """
        instance = Game()
        ui = instance.ui
        self.assertTrue(isinstance(ui, UserInterface), "Should have a user interface object")

    def test_live_player(self):
        """
        Make sure the LivePlayer object always reports True when asked
        :return:
        """
        instance = LivePlayer()
        self.assertTrue(instance.is_alive(), "Should believe itself to be alive")

    def test_dead_player(self):
        """
        Make sure the DeadPlayer object always reports False when asked
        :return:
        """
        instance = DeadPlayer()
        self.assertFalse(instance.is_alive(), "Should believe itself to be dead")

    def test_game_still_active_two_live(self):
        """
        Test game still active == true when both are live
        :return:
        """
        instance = Game()
        instance.player_one = LivePlayer()
        instance.computer = LivePlayer()
        self.assertTrue(instance.game_still_active(), "Should be true")

    def test_game_still_active_two_dead(self):
        """
        Test game_still_active() == false when both are dead
        :return:
        """
        instance = Game()
        instance.player_one = DeadPlayer()
        instance.computer = DeadPlayer()
        self.assertFalse(instance.game_still_active(), "Should be false - game over")

    def test_game_still_active_p1_live_comp_dead(self):
        """
        Test game_still_active() == false when player_one is live
         and computer is dead
        :return:
        """
        instance = Game()
        instance.player_one = LivePlayer()
        instance.computer = DeadPlayer()
        self.assertFalse(instance.game_still_active(), "Should be false - game over")

    def test_game_still_active_p1_dead_comp_live(self):
        """
        Test game_still_active() == false when player_one is dead
         and computer is live
        :return:
        """
        instance = Game()
        instance.player_one = DeadPlayer()
        instance.computer = LivePlayer()
        self.assertFalse(instance.game_still_active(), "Should be false - game over")


class LivePlayer(Player):
    """
    Test object to always report True when asked if alive
    """

    def __init__(self):
        Player.__init__(self, 'Live')

    def is_alive(self):
        return True


class DeadPlayer(Player):
    """
    Test object to always report True when asked if alive
    """

    def __init__(self):
        Player.__init__(self, 'Dead')

    def is_alive(self):
        return False
