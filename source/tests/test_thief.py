from unittest import TestCase
from card import Thief


class TestThief(TestCase):
    def test___init__(self):
        instance = Thief()
        self.assertEqual('Thief', instance.name, 'Name should be "Thief" but was "' + instance.name + '"')
        self.assertEqual(1, instance.strength, 'Strength of a/an Thief should be 1 but was ' + str(instance.strength))
        self.assertEqual(1, instance.value, 'Value of a/an Thief should be 1 but was ' + str(instance.value))
        self.assertEqual(1, instance.cost, 'Cost of a/an Thief should be 1 but was ' + str(instance.cost))

