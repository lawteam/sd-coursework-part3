from unittest import TestCase
from card import Crossbowman


class TestCrossbowman(TestCase):
    def test___init__(self):
        instance = Crossbowman()
        self.assertEqual('Crossbowman', instance.name, 'Name should be "Crossbowman" but was "' + instance.name + '"')
        self.assertEqual(4, instance.strength, 'Strength of a/an Crossbowman should be 4 but was ' + str(instance.strength))
        self.assertEqual(0, instance.value, 'Value of a/an Crossbowman should be 0 but was ' + str(instance.value))
        self.assertEqual(3, instance.cost, 'Cost of a/an Crossbowman should be 3 but was ' + str(instance.cost))

