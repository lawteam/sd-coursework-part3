from unittest import TestCase
from card import Card


class TestCard(TestCase):
    def test_immutable_name(self):
        instance = Card("Name", 1, 1, 1)
        try:
            instance.name = "new name"
        except NotImplementedError:
            pass
        else:
            self.fail("Should have thrown an error")

    def test_immutable_strength(self):
        instance = Card("Name", 1, 1, 1)
        try:
            instance.strength = 99
        except NotImplementedError:
            pass
        else:
            self.fail("Should have thrown an error")

    def test_immutable_value(self):
        instance = Card("Name", 1, 1, 1)
        try:
            instance.value = 107
        except NotImplementedError:
            pass
        else:
            self.fail("Should have thrown an error")

    def test_immutable_cost(self):
        instance = Card("Name", 1, 1, 1)
        try:
            instance.cost = 13
        except NotImplementedError:
            pass
        else:
            self.fail("Should have thrown an error")
