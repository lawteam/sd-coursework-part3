from unittest import TestCase
from card import Swordsman


class TestSwordsman(TestCase):
    def test___init__(self):
        instance = Swordsman()
        self.assertEqual('Swordsman', instance.name, 'Name should be "Swordsman" but was "' + instance.name + '"')
        self.assertEqual(4, instance.strength, 'Strength of a/an Swordsman should be 4 but was ' + str(instance.strength))
        self.assertEqual(0, instance.value, 'Value of a/an Swordsman should be 0 but was ' + str(instance.value))
        self.assertEqual(3, instance.cost, 'Cost of a/an Swordsman should be 3 but was ' + str(instance.cost))

