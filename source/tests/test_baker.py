from unittest import TestCase
from card import Baker


class TestBaker(TestCase):
    def test___init__(self):
        instance = Baker()
        self.assertEqual('Baker', instance.name, 'Name should be "Baker" but was "' + instance.name + '"')
        self.assertEqual(0, instance.strength, 'Strength of a/an Baker should be 0 but was ' + str(instance.strength))
        self.assertEqual(3, instance.value, 'Value of a/an Baker should be 3 but was ' + str(instance.value))
        self.assertEqual(2, instance.cost, 'Cost of a/an Baker should be 2 but was ' + str(instance.cost))

