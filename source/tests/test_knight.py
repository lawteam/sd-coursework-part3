from unittest import TestCase
from card import Knight


class TestKnight(TestCase):
    def test___init__(self):
        instance = Knight()
        self.assertEqual('Knight', instance.name, 'Name should be "Knight" but was "' + instance.name + '"')
        self.assertEqual(6, instance.strength, 'Strength of a/an Knight should be 6 but was ' + str(instance.strength))
        self.assertEqual(0, instance.value, 'Value of a/an Knight should be 0 but was ' + str(instance.value))
        self.assertEqual(5, instance.cost, 'Cost of a/an Knight should be 5 but was ' + str(instance.cost))

