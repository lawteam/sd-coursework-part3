from unittest import TestCase
from card import Caravan


class TestCaravan(TestCase):
    def test___init__(self):
        instance = Caravan()
        self.assertEqual('Caravan', instance.name, 'Name should be "Caravan" but was "' + instance.name + '"')
        self.assertEqual(1, instance.strength, 'Strength of a/an Caravan should be 1 but was ' + str(instance.strength))
        self.assertEqual(5, instance.value, 'Value of a/an Caravan should be 5 but was ' + str(instance.value))
        self.assertEqual(5, instance.cost, 'Cost of a/an Caravan should be 5 but was ' + str(instance.cost))

