from unittest import TestCase
from player import Player, LogicError
from card import Squire, Serf, Tailor


class TestPlayer(TestCase):

    def test___init__(self):
        instance = Player('Player Name')
        self.assertEqual('Player Name', instance.name, "Expected name to be 'Player Name'")
        self.assertEqual(30, instance.health, "Expected initial health to be 30")

    def test_can_reduce_health(self):
        instance = Player('Other Name')
        instance.reduce_health(20)
        self.assertEqual(10, instance.health, "Expected health to be 10")


    def test_can_reduce_health_only_to_zero(self):
        instance = Player('Other Name')
        instance.reduce_health(99)
        self.assertEqual(0, instance.health, "Expected final health to be 0")

    def test_alive_by_default(self):
        instance = Player('live object')
        self.assertTrue(instance.is_alive(), "Should be alive")

    def test_dies_at_zero(self):
        instance = Player('live object')
        instance.reduce_health(99)
        self.assertFalse(instance.is_alive(), "Should be dead")

    def test_has_deck(self):
        instance = Player('test deck')
        self.assertIsNotNone(instance.deck)

    def test_deck_has_expected_contents(self):
        instance = Player('test deck contents')
        deck = instance.deck
        serfs = 0
        squires = 0
        for card in deck:
            if isinstance(card, Serf):
                serfs += 1
            elif isinstance(card, Squire):
                squires += 1
        self.assertEqual((serfs + squires), 10, "Player Deck didn't contain the expected number of cards")
        self.assertEqual(serfs, 8, "Player Deck didn't contain expected number of Serfs")
        self.assertEqual(squires, 2, "Player Deck didn't contain expected number of Squires")

    def test_deck_randomness(self):
        """
        Best effort at checking that we have a shuffling routine. Make 1000 Player objects
        one after the other and count how many times we see Squire cards at each of the 10 slots in
        the deck
        If one slot sees fewer than 160 allocations then warn that we may be broken

        This test will *occasionally* fail because randomness is - well - random
        :return:
        """
        squire_loc = [0] * 10
        for x in range(0, 1000):
            instance = Player('fake')
            deck = instance.deck
            self.assertEqual(10, len(deck), "Deck did not contain 10 cards")
            squire_count = 0
            for idx in range(0,10):
                if isinstance(deck[idx], Squire):
                    squire_count += 1
                    squire_loc[idx] += 1
            self.assertEqual(2, squire_count, "Deck didn't have 2 squires")
        fail = False
        fail_string = ""
        for idx in range(0,10):
            if (squire_loc[idx] < 160):
                fail = True
                fail_string += "\nRandomness of the deck may be broken - only saw a Squire at location " + str(idx) + " on " + str(squire_loc[idx]) + " occasions"
        if (fail):
            self.fail(fail_string)

    def test_has_discard(self):
        """
        A Player object has an empty Discard set
        :return:
        """
        instance = Player('test discard')
        discard = instance.discard
        self.assertTrue(len(discard) == 0, "Discard pile should be empty")

    def test_has_hand(self):
        """
        A Player object has an empty Hand set
        :return:
        """
        instance = Player('test hand')
        hand = instance.hand
        self.assertTrue(len(hand) == 0, "Hand should be empty")

    def test_has_active(self):
        """
        A Player object has an empty Active set
        :return:
        """
        instance = Player('test active')
        active = instance.active
        self.assertTrue(len(active) == 0, "Active pile should be empty")

    def test_active_attack_strength_is_zero(self):
        """
        The initial attack strength of the Active set is zero
        :return:
        """
        instance = Player('test active')
        attack_strength = instance.attack_strength
        self.assertEquals(0, attack_strength, "Active attack strength should be zero")

    def test_active_monetary_value_is_zero(self):
        """
        The initial attack strength of the Active set is zero
        :return:
        """
        instance = Player('test active')
        monetary_value = instance.attack_strength
        self.assertEquals(0, monetary_value, "Active monetary value should be zero")


    def test_in_play_false(self):
        """
        A brand new Player object is not in a turn
        :return:
        """
        instance = Player('test in play')
        self.assertFalse(instance.in_play, 'Should not be in play by default')

    def test_turn_can_start(self):
        """
        If we begin a turn then we should have noted that fact
        :return:
        """
        instance = Player('test turn start')
        instance.begin_turn()
        self.assertTrue(instance.in_play, 'Should be in play after starting turn')

    def test_turn_start_fills_hand(self):
        """
        If we begin a turn, cards are removed from the top of the Deck and put into the Hand
        :return:
        """
        my_deck = [Tailor()] * 8
        instance = Player('test hand setting')
        instance.deck = my_deck
        instance.begin_turn()
        hand = instance.hand
        self.assertEqual(5, len(hand), "Hand should now contain 5 cards")
        deck = instance.deck
        self.assertEqual(3, len(deck), "Deck should now contain 3 cards")


    def test_turn_over_turn_is_fail(self):
        """
        If we are in a turn and we try to begin another turn then we get a LogicError
        :return:
        """
        instance = Player('test turn over turn')
        instance.begin_turn()
        try:
            instance.begin_turn()
        except LogicError:
            pass
        else:
            self.fail("Should have thrown an error")

    def test_end_turn_when_not_in_turn_is_fail(self):
        """
        If we are not in a turn and we try to end the non-existent turn then we get a LogicError
        :return:
        """
        instance = Player('test end turn no turn')
        try:
            instance.end_turn()
        except LogicError:
            pass
        else:
            self.fail("Should have thrown an error")


    def test_start_hand_with_short_deck(self):
        """
        If we have fewer than 5 cards in the Deck then we pull those off into the Hand anyway,
        then we shuffle the Discard pile, move that to the Deck and pull the remainder of the cards from
        that.
        Afterwards, the Hand should be 5 cards long, the Discard should be empty and the Deck should
        then have the remainder of the cards in it
        :return:
        """
        instance = Player('short deck')
        my_deck = [Tailor()] * 3
        my_discard = [Serf()] * 20
        instance.deck = my_deck
        instance.discard = my_discard
        self.assertEqual(3, len(instance.deck), "Deck should be 3 long")
        self.assertEqual(20, len(instance.discard), "Discard should be 20 long")
        self.assertEqual(0, len(instance.hand), "Hand should be empty")
        instance.begin_turn()
        self.assertEqual(18, len(instance.deck), "Deck should be 18 long")
        self.assertEqual(0, len(instance.discard), "Discard should be empty")
        self.assertEqual(5, len(instance.hand), "Hand should be full (5 cards)")


    def test_end_turn(self):
        """
        When we end a turn, all the cards from the Deck and all the cards from the Active
        set go into the Discard pile, the attack_strength and the onetary_value get set back to zero and
        we note that we are no longer in a turn
        :return:
        """
        instance = Player('end turn')
        my_deck = [Tailor()] * 5
        my_discard = [Serf()] * 12
        my_hand = [Squire()] * 3
        my_active = [Serf()] * 2
        instance.deck = my_deck
        instance.discard = my_discard
        instance.hand = my_hand
        instance.active = my_active
        instance.attack_strength = 4
        instance.monetary_value = 6
        instance.in_play = True
        instance.end_turn()
        self.assertEqual(5, len(instance.deck), "Deck should now contain 5 cards")
        self.assertEqual(0, len(instance.hand), "Hand should now be empty")
        self.assertEqual(0, len(instance.active), "Active should now be empty")
        self.assertEqual(17, len(instance.discard), "Discard should now contain 17 cards")
        self.assertEqual(0, instance.attack_strength, "Attack strength should now be zero")
        self.assertEqual(0, instance.monetary_value, "Monetary value should now be zero")
        self.assertFalse(instance.in_play, "Should now not be in play")


    def test_play_card_valid(self):
        """
        Given a Player where the hand is in_play we can transfer a card from the Hand
        to the Active set by index number. Its strength and monetary value contribute
        to the values of the active hand
        :return:
        """
        my_deck = [Tailor(), Serf(), Squire(), Serf(), Serf(), Tailor(), Squire(), Serf()]
        instance = Player('play card')
        instance.deck = my_deck
        instance.begin_turn()
        instance.play_card(0)
        hand = instance.hand
        self.assertEqual(4, len(hand), "Hand should now contain 4 cards")
        deck = instance.deck
        self.assertEqual(3, len(deck), "Deck should now contain 3 cards")
        active = instance.active
        self.assertEqual(1, len(active), "Active set should now contain 1 card")
        self.assertEqual(0, instance.attack_strength, "attack_strength should be zero")
        self.assertEqual(1, instance.monetary_value, "monetary value should be 1 ")


    def test_play_card_two_different_valid(self):
        """
        Given a Player where the hand is in_play we can transfer a card from the Hand
        to the Active set by index number. Its strength and monetary value contribute
        to the values of the active hand
        A second card adds to the values.
        :return:
       """
        my_deck = [Tailor(), Serf(), Squire(), Serf(), Serf(), Tailor(), Squire(), Serf()]
        instance = Player('play card')
        instance.deck = my_deck
        instance.begin_turn()
        instance.play_card(0)
        instance.play_card(0)
        hand = instance.hand
        self.assertEqual(3, len(hand), "Hand should now contain 3 cards")
        deck = instance.deck
        self.assertEqual(3, len(deck), "Deck should now contain 3 cards")
        active = instance.active
        self.assertEqual(2, len(active), "Active set should now contain 2 cards")
        self.assertEqual(1, instance.attack_strength, "attack_strength should be 1")
        self.assertEqual(1, instance.monetary_value, "monetary value should be 1 ")

