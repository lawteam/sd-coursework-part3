from unittest import TestCase
from card import Thug


class TestThug(TestCase):
    def test___init__(self):
        instance = Thug()
        self.assertEqual('Thug', instance.name, 'Name should be "Thug" but was "' + instance.name + '"')
        self.assertEqual(2, instance.strength, 'Strength of a/an Thug should be 2 but was ' + str(instance.strength))
        self.assertEqual(0, instance.value, 'Value of a/an Thug should be 0 but was ' + str(instance.value))
        self.assertEqual(1, instance.cost, 'Cost of a/an Thug should be 1 but was ' + str(instance.cost))

