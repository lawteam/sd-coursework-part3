from unittest import TestCase
from card import Tailor


class TestTailor(TestCase):
    def test___init__(self):
        instance = Tailor()
        self.assertEqual('Tailor', instance.name, 'Name should be "Tailor" but was "' + instance.name + '"')
        self.assertEqual(0, instance.strength, 'Strength of a/an Tailor should be 0 but was ' + str(instance.strength))
        self.assertEqual(4, instance.value, 'Value of a/an Tailor should be 4 but was ' + str(instance.value))
        self.assertEqual(3, instance.cost, 'Cost of a/an Tailor should be 3 but was ' + str(instance.cost))

