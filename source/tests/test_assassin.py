from unittest import TestCase
from card import Assassin


class TestAssassin(TestCase):
    def test___init__(self):
        instance = Assassin()
        self.assertEqual('Assassin', instance.name, 'Name should be "Assassin" but was "' + instance.name + '"')
        self.assertEqual(5, instance.strength, 'Strength of a/an Assassin should be 5 but was ' + str(instance.strength))
        self.assertEqual(0, instance.value, 'Value of a/an Assassin should be 0 but was ' + str(instance.value))
        self.assertEqual(4, instance.cost, 'Cost of a/an Assassin should be 4 but was ' + str(instance.cost))

