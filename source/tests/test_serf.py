from unittest import TestCase
from card import Serf


class TestSerf(TestCase):
    def test___init__(self):
        instance = Serf()
        self.assertEqual('Serf', instance.name, 'Name should be "Serf" but was "' + instance.name + '"')
        self.assertEqual(0, instance.strength, 'Strength of a/an Serf should be 0 but was ' + str(instance.strength))
        self.assertEqual(1, instance.value, 'Value of a/an Serf should be 1 but was ' + str(instance.value))
        self.assertEqual(0, instance.cost, 'Cost of a/an Serf should be 0 but was ' + str(instance.cost))

