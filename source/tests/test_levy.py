from unittest import TestCase
from card import Levy


class TestLevy(TestCase):
    def test___init__(self):
        instance = Levy()
        self.assertEqual('Levy', instance.name, 'Name should be "Levy" but was "' + instance.name + '"')
        self.assertEqual(1, instance.strength, 'Strength of a/an Levy should be 1 but was ' + str(instance.strength))
        self.assertEqual(2, instance.value, 'Value of a/an Levy should be 2 but was ' + str(instance.value))
        self.assertEqual(2, instance.cost, 'Cost of a/an Levy should be 2 but was ' + str(instance.cost))

