from unittest import TestCase
from card import Merchant


class TestMerchant(TestCase):
    def test___init__(self):
        instance = Merchant()
        self.assertEqual('Merchant', instance.name, 'Name should be "Merchant" but was "' + instance.name + '"')
        self.assertEqual(0, instance.strength, 'Strength of a/an Merchant should be 0 but was ' + str(instance.strength))
        self.assertEqual(5, instance.value, 'Value of a/an Merchant should be 5 but was ' + str(instance.value))
        self.assertEqual(4, instance.cost, 'Cost of a/an Merchant should be 4 but was ' + str(instance.cost))

