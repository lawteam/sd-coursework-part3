from unittest import TestCase
from card import Archer


class TestArcher(TestCase):
    def test___init__(self):
        instance = Archer()
        self.assertEqual('Archer', instance.name, 'Name should be "Archer" but was "' + instance.name + '"')
        self.assertEqual(3, instance.strength, 'Strength of a/an Archer should be 3 but was ' + str(instance.strength))
        self.assertEqual(0, instance.value, 'Value of a/an Archer should be 0 but was ' + str(instance.value))
        self.assertEqual(2, instance.cost, 'Cost of a/an Archer should be 2 but was ' + str(instance.cost))

