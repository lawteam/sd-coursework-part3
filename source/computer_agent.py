from user_interface import UserInterface
from time import sleep


class ComputerAgent(UserInterface):
    def __init__(self, player, game):
        """
        Stash the player and the game objects away for safe-keeping. The player object is
        required so we can access the hand/active and attack strength values. The game
        object is required so we can identify the options that we have for purchasing
        :type game: Game
        :param game: The game in which this agent is involved
        :type player: Player
        :param player: the computer player that this agent is driving
        :return:
        """
        self.player = player
        self.game = game
        self.style = 'A'
        self.delay = 2

    def get_options_response(self, prompt, options, acceptables):
        """
        Calculate the next action and return it
        :param prompt:
        :param options:
        :param acceptables:
        :return:
        """
        self._report_("Thinking...")
        if len(self.player.hand) > 0:
            self._report_choice_("Play All Cards")
            return 'P'

        if self.player.attack_strength > 0:
            self._report_choice_("Attack")
            return 'A'

        chosen_card = self.chosen_card()
        if chosen_card is not None:
            self._report_choice_("Purchase a %s card" % chosen_card.card.name)
            return chosen_card.action

        self._report_choice_("End Turn")
        return 'E'

    def chosen_card(self):
        """
        Pick a card to purchase based on attack strength.
        :return: the index of the card in the central line
        """
        best_score = -1
        best_action = ''
        best_card = None
        central = self.game.central
        for x in range(0, len(central)):
            card = central[x]
            if card.cost <= self.player.monetary_value:
                value = self.get_card_value(card)
                if value > best_score:
                    best_score = value
                    best_card = card
                    best_action = 'B%d' % x
        supplement = self.game.supplement
        if len(supplement) > 0:
            card = supplement[0]
            if card.cost <= self.player.monetary_value:
                if self.get_card_value(card) > best_score:
                    best_card = card
                    best_action = 'S'

        return ChosenCard(best_card, best_action) if best_card is not None else None

    def get_card_value(self, card):
        """
        Pick a card to purchase based on attack strength
        :param card:
        :return: the index of the card in the central line
        """
        if self.style == 'A':
            return card.strength
        else:
            return card.value

    def _report_(self, message):
        """
        Print the supplied message, wait a few seconds then return
        :param message:
        :return:
        """
        print
        print message
        sleep(self.delay)
        return

    def _report_choice_(self, choice):
        """
        Report the computer's chose action
        :param choice:
        :return:
        """
        message = "Computer chose to %s" % choice
        self._report_(message)


class ChosenCard(object):
    def __init__(self, card, action):
        """
        Create a ChosenCard tuple object. This is used to pass back the chosen card
        and the string action to which it corresponds
        :param card:
        :param action:
        :return:
        """
        self.card = card
        self.action = action
