class Card(object):
    def __init__(self, name, strength, value, cost):
        """

        :rtype: Card
        """
        super(Card, self).__setattr__("name", name)
        super(Card, self).__setattr__("cost", cost)
        super(Card, self).__setattr__("strength", strength)
        super(Card, self).__setattr__("value", value)

    def __setattr__(self, *ignored):
        raise NotImplementedError

    __delattr__ = __setattr__


class Archer(Card):
    def __init__(self):
        Card.__init__(self, "Archer", 3, 0, 2)


class Assassin(Card):
    def __init__(self):
        Card.__init__(self, "Assassin", 5, 0, 4)


class Baker(Card):
    def __init__(self):
        Card.__init__(self, "Baker", 0, 3, 2)


class Caravan(Card):
    def __init__(self):
        Card.__init__(self, "Caravan", 1, 5, 5)


class Catapault(Card):
    def __init__(self):
        Card.__init__(self, "Catapault", 7, 0, 6)


class Crossbowman(Card):
    def __init__(self):
        Card.__init__(self, "Crossbowman", 4, 0, 3)


class Knight(Card):
    def __init__(self):
        Card.__init__(self, "Knight", 6, 0, 5)


class Levy(Card):
    def __init__(self):
        Card.__init__(self, "Levy", 1, 2, 2)


class Merchant(Card):
    def __init__(self):
        Card.__init__(self, "Merchant", 0, 5, 4)


class Serf(Card):
    def __init__(self):
        Card.__init__(self, "Serf", 0, 1, 0)


class Squire(Card):
    def __init__(self):
        Card.__init__(self, "Squire", 1, 0, 0)


class Swordsman(Card):
    def __init__(self):
        Card.__init__(self, "Swordsman", 4, 0, 3)


class Tailor(Card):
    def __init__(self):
        Card.__init__(self, "Tailor", 0, 4, 3)


class Thief(Card):
    def __init__(self):
        Card.__init__(self, "Thief", 1, 1, 1)


class Thug(Card):
    def __init__(self):
        Card.__init__(self, "Thug", 2, 0, 1)
