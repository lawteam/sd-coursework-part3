Part 3 of the Software Development module assessment submission.

Tests can be run by issuing the command 'make' when inside the source/ directory.

The game can be run by issuing the command ./run_dbc from inside the top-level
directory (the one that contains this README file)
